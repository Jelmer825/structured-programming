import random
def printPyramidFor():
    pyramid = int(input('Enter a number: '))

    print("For loop pyramids: ")
    # Pyramid left right
    for x in range(pyramid+1):
        print("* "*x)

    for x in range(pyramid-1, -1, -1):
        print("* "*x)

    # Pyramid right left
    for i in range(pyramid+1):
        # Add spaces
        for j in range(pyramid-i):
            print("  ", end="")
        print(" *"*i)

    for i in range(pyramid-1, -1, -1):
        # Add spaces
        for j in range(pyramid-i):
            print("  ", end="")
        print(" *"*i)

def printPyramidWhile():
    pyramid = int(input('Enter a number: '))
    i = 0

    print("While loop pyramids: ")

    # Pyramid left right
    while i <= pyramid:
        print("* "*i)
        i = i + 1

    i = 0
    temp = pyramid
    while i <= temp - 1:
        print("* " * (temp-1))
        temp = temp - 1

    # Pyramid right left
    i = 1
    while i <= pyramid:
        j = pyramid - i
        while j > 0:
            print("  ", end="")
            j -= 1
        print(" *"*i)
        i += 1

    i = pyramid -1
    while 0 < i:
        j = pyramid - i
        while j > 0:
            print("  ", end="")
            j -= 1
        print(" *" * i)
        i -= 1

def sentenceCheck():
    sentenceOne = input('First sentence: ')
    sentenceTwo = input('Second sentence: ')

    for x in range(len(sentenceOne)-1):
        if sentenceTwo[x] != sentenceOne[x]:
            print(f'The first difference is found at index: {x+1}')
            break

def count(x, lst):
    counter = 0
    for i in range(len(lst)-1):
        if lst[i] == x:
            counter+=1

    print(counter)
    return counter

def lstDiff(lst):
    highest = 0
    for x in range(len(lst)-1):
        if (lst[x] + lst[x+1]) > highest:
            highest = (lst[x] + lst[x+1])

    print(highest)
    return highest

def oneCounter(lst):
    oneCount = count(1, lst)
    zeroCount = count(0, lst)

    if oneCount > zeroCount:
        if zeroCount <= 12:
            return True
        else:
            return False
    else:
        return False

def palindroom(word):
    result = list(reversed(word))
    for x in range(len(result)):
        if word[x] != result[x]:
            return 'Dit is geen palindroom'

    return 'Dit is wel een palindroom'

def palindroom2(word):
    result = word[::-1]
    for x in range(len(result)):
        if word[x] != result[x]:
            return 'Dit is geen palindroom'

    return 'Dit is wel een palindroom'

def swap(lst, i, j):
    temp = lst[i]
    lst[i] = lst[j]
    lst[j] = temp

def quickSort(lst, low, high):
    if low < high:
        pivot_location = partition(lst, low, high)
        quickSort(lst, low, pivot_location)
        quickSort(lst, pivot_location+1, high)

def partition(lst, low, high):
    pivot = lst[low]
    left = low

    for i in range(low+1, high):
        if lst[i] < pivot:
            swap(lst, i, left+1)
            left += 1

    swap(lst, low, left)

    return

def avarage(lst):
    total = 0
    for x in lst:
        total += x
    return round(total / len(lst), 2)

def avarageLst(lst):
    avList = []
    for x in lst:
        avList.append(avarage(x))
    return avList

def randomGuess():
    rand = random.randint(0, 10)

    while True:
        guess = int(input('Guess the number: '))
        if guess == rand:
            return "Correct!"

def compress():
    inputFile = open("file", "r")
    exportFile = open("compressed", "w")
    for line in inputFile:
        if not line.isspace():
            exportFile.write(line.strip(' '))

    inputFile.close()
    exportFile.close()

def cyclicMovement(ch, n):
    for x in range(abs(n)):
        if n > 0:
            ch = ch + ch[0]
            ch = ch[1::]
        elif n < 0:
            ch = ch[-1] + ch
            ch = ch[:-1]
    return ch

def fibonaci(lst, n, num):
    if n <= 1:
        return lst[-1]

    sum = lst[-1] + lst[-2]
    if num != 0:
        lst.append(sum)
    return fibonaci(lst, n-1, sum)

def caesar():
    # Set base alphabet
    alphabetChar = "abcdefghijklmnopqrstuvwxyz"
    # Get number for each letter
    alphabetNum = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]

    # Get text and rotations
    text = input("The text is: ").lower()
    rotation = int(input("The rotations are: "))

    # Get all characters from text
    textNums = []
    for x in text:
        if x == " ":
            textNums.append(" ")
        else:
            textNums.append(alphabetNum[alphabetChar.index(x)-1])

    # Rotate alphabet
    rotate = cyclicMovement(alphabetChar, rotation)

    # Encrypt text to the rotated alphabet
    encrypted = ""
    for x in textNums:
        if x == " ":
            encrypted += " "
        else:
            encrypted += rotate[x]

    print(text)
    print(encrypted)
    return encrypted

def fizz():
    for x in range(100):
        if x % 3 and x % 5 == 0:
            print('FizzBuzz')

        elif x % 3 == 0:
            print('Fizz')
        elif x % 5 == 0:
            print('Buzz')
        else:
            print(x)
# printPyramidFor()
# printPyramidWhile()
# sentenceCheck()
# lstDiff([20,12,4,5,8,3,2,1,3,9,10])
# print(oneCounter([1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0]))
# print(palindroom('job'))
# print(palindroom2('bob'))
# Quicksort
# lst = random.sample(range(1,1000), 999)
# quickSort(lst, 0, len(lst))
# print(lst)
# ===
# print(avarage([1,2, 8, 9, 10, 10, 6, 7, 7.5]))
# print(avarageLst([[1,2,3], [5,6,1], [10,10,2]]))
# print(randomGuess())
# compress()
# print(cyclicMovement("1011100", -4))
# print(fibonaci([0, 1], 28, 1))
# caesar()
# fizz()