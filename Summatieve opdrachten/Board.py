import random, time
from Bot import *
from Player import *
class Board:

    # Times guessed
    turn = 0

    # Number of guesses left
    totalGuesses = 8

    # Secret code hidden
    secretCodeHidden = ["HIDDEN", "HIDDEN", "HIDDEN", "HIDDEN"]

    # List of all guesses.
    guessCodes = [['-', '-', '-', '-'] for x in range(totalGuesses)]

    # List of feedback flags
    guessFlags = [['-', '-', '-', '-'] for i in range(totalGuesses)]

    # Feedback for the AI
    feedback = [0, 0]

    isPlayer = None

    avarage = 0

    won = False

    # Constructor.
    def __init__(self, secretCode, colors, colorCodes, isPlayer, algo=None):
        self.secretCode = secretCode,
        self.isPlayer = isPlayer,
        self.colors = colors,
        self.colorCodes = colorCodes
        self.algo = algo

        # Set player object and play the game...
        if self.isPlayer[0]:
            self.player = Player()
        else:
            self.bot = Bot()

    # Print board
    def printBoard(self):
        print("-----------------------------------------")
        print("\tMASTERMIND Hogeschool Utrecht")
        print("-----------------------------------------")
        print("    |", end="")
        for x in self.secretCodeHidden:
            print("\t" + x[:3], end="")
        print()

        # Print guesses reversed so the last guesses will be at the end of the board.
        for x in reversed(range(len(self.guessCodes))):
            print("-----------------------------------------")
            print(self.guessFlags[x][0], self.guessFlags[x][1], "|")

            print(self.guessFlags[x][2], self.guessFlags[x][3], end=" |")
            for x in self.guessCodes[x]:
                print("\t" + x[:3], end="")
            print()

        print("-----------------------------------------")
        print("\tDoor Jelmer Hilhorst")
        print("-----------------------------------------")

    def doGuess(self):
        # Print the menu
        # print("-----------------------------------------")
        # print("\t\t\t\t   Menu")
        # print("-----------------------------------------")
        # print("Enter your guess with the numbers below:\n")
        # print("1 - GREEN, 2 - RED, 3 - BLUE, 4 - YELLOW,\n5 - BLACK, 6 - PINK\n")
        # print("Format your guess as follows: 1 2 3 4 \n\t\t Remember the spaces!")
        # print("-----------------------------------------")

        # Check if a person is playing or the AI is playing
        if self.isPlayer[0]:

            # Make guess
            self.addGuess(self.player.inputGuess())

            # Check if the player won the game
            self.checkWin()

            # Print the board with the latest guess and feedback added
            self.printBoard()

            # Add 1 to the turn.
            self.turn += 1

        else:
            # print("Thinking....")
            # time.sleep(2)
            # AI makes guess
            self.addGuess(self.bot.botGuess(self.feedback, self.turn, self.algo))

            # Check if the player won the game
            self.checkWin()

            # Print the board with the latest guess and feedback added
            # self.printBoard()

            # Add 1 to the turn.
            self.turn += 1

    """
        Add guess to the board and get the feedback.
    """
    def addGuess(self, guess):
        # Set counter to add color at right place
        counter = 0
        for x in guess:
            self.guessCodes[self.turn][counter] = self.colorCodes[x]
            counter += 1

        # Set feedback:
        self.feedback = self.giveFeedback(guess)

    """
        Check if the game is won.
    """
    def checkWin(self):
        if self.guessCodes[self.turn] == self.secretCode[0]:
            self.secretCodeHidden = self.secretCode[0]
            # self.printBoard()
            # print(f"Good game! You won the game in {self.turn+1} turn(s)!")
            # quit("Quitting program...")
            self.avarage += self.turn+1
            self.won = True

    """
        Give feedback based on the guess.
    """
    def giveFeedback(self, guess):

        # Set feedback
        feedback = [0, 0]

        # Copy secret code to modify it without consequence
        copyPass = [x for x in self.secretCode[0]]

        # Position at feedback board
        pos = 0

        for x in guess:
            if self.colorCodes[x] in copyPass:
                if guess.index(x) == list(self.secretCode[0]).index(self.colorCodes[x]):
                    self.generateFlag(pos, True)
                    feedback[0] += 1
                else:
                    self.generateFlag(pos, False)
                    feedback[1] += 1
                pos += 1
                # Remove color from copyPass to prevent redundancy.
                copyPass.remove(self.colorCodes[x])

        random.shuffle(self.guessFlags[self.turn])
        return feedback

    """
        Generate flags for board
    """
    def generateFlag(self, pos, black):
        if black:
            self.guessFlags[self.turn][pos] = 'B'
        else:
            self.guessFlags[self.turn][pos] = 'W'
