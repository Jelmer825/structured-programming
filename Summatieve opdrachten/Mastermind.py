import random
import os
from Board import *
from Player import *
from Bot import *

def isPlayer():
    print("-----------------------------------------")
    print("Start number:\n1 to play yourself.\n2 to let the AI play. \n3 to quit.")
    print("-----------------------------------------")
    while True:
        try:
            choice = int(input("Enter a number: "))
            if choice == 1:
                return True
            elif choice == 2:
                return False
            elif choice == 3:
                print("Shutting down system")
                quit()
        except ValueError:
            print("Enter a valid number!")

def genCode():
    print("-----------------------------------------")
    print("\t\t\t\t   Menu")
    print("-----------------------------------------")
    print("Enter your secret code with the numbers below:\n")
    print("1 - GREEN, 2 - RED, 3 - BLUE, 4 - YELLOW,\n5 - BLACK, 6 - PINK\n")
    print("Format your secret as follows: 1 2 3 4 \n\t\t Remember the spaces!")
    print("-----------------------------------------")

    while True:
        # Accepting the player input
        try:
            secret = list(map(int, input("Enter your secret = ").split()))

            correct = True
            for color in secret:
                if color > 6 or color < 1:
                    correct = False
                    print("\tError! Please add a valid secret code!")

            # Check if guess has a valid length.
            if len(secret) != 4:
                correct = False
                print("\tError! Please add a valid secret code!")

            if correct:
                return secret

        except ValueError:
            print("\tError! Please add a valid secret code!")
            continue

def pickAlgoritm():
    print("-----------------------------------------")
    print("\t\t\t\t   Menu")
    print("-----------------------------------------")
    print("Pick your algorithm with the numbers below:\n")
    print("1 - Simple algorithm, 2 - Worst case algorithm, 3 - Expected size algorithm, 4 - Own algorithm")
    print("-----------------------------------------")
    choice = int(input("Choose your algorithm: "))
    return choice

if __name__ == '__main__':
    colors = ['GREEN', 'RED', 'BLUE', 'YELLOW', 'BLACK', 'PINK']

    # Mapping of colors to numbers
    colorCodes = {1: "GREEN", 2: "RED", 3: "BLUE", 4: "YELLOW", 5: "BLACK", 6: "PINK"}

    # Set player or bot
    isPlayer = isPlayer()

    # Randomly generate code or let the player pick a code
    if isPlayer:
        # Randomly get a secret code
        random.shuffle(colors)
        secretCode = colors[:4]
        algo = None
    else:
        secretInput = genCode()

        secretCode = []
        for x in secretInput:
            secretCode.append(colorCodes[x])

        algo = pickAlgoritm()

    avarage = 0
    turns = 0
    for x in range(100):
        random.shuffle(colors)
        secretCode = colors[:4]
        # Set board object
        board = Board(secretCode, colors, colorCodes, isPlayer, algo)

        # Print board
        # board.printBoard()
        while board.turn < board.totalGuesses:
            board.doGuess()
            if board.won:
                break

        avarage += board.avarage
        turns +=1

        # print("YOU ARE A LOSER!!!!!!!!!!!")
        # quit("Quitting program...")
    print(avarage / turns)


