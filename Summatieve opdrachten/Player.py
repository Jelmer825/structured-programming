class Player():
    def inputGuess(self):
        while True:
            # Accepting the player input
            try:
                guess = list(map(int, input("Enter your guess = ").split()))

                correct = True
                for color in guess:
                    if color > 6 or color < 1:
                        correct = False
                        print("\tError! Please add a valid guess code!")

                # Check if guess has a valid length.
                if len(guess) != 4:
                    correct = False
                    print("\tError! Please add a valid guess code!")

                if correct:
                    return guess

            except ValueError:
                print("\tError! Please add a valid guess code!")
                continue
